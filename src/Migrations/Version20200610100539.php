<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200610100539 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE office (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, name VARCHAR(10) NOT NULL, size INT NOT NULL, INDEX IDX_74516B02979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE office_member (office_id INT NOT NULL, member_id INT NOT NULL, INDEX IDX_9D5522FBFFA0C224 (office_id), INDEX IDX_9D5522FB7597D3FE (member_id), PRIMARY KEY(office_id, member_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE office ADD CONSTRAINT FK_74516B02979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE office_member ADD CONSTRAINT FK_9D5522FBFFA0C224 FOREIGN KEY (office_id) REFERENCES office (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE office_member ADD CONSTRAINT FK_9D5522FB7597D3FE FOREIGN KEY (member_id) REFERENCES member (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company CHANGE nb_employees nb_employees INT DEFAULT NULL');
        $this->addSql('ALTER TABLE member CHANGE company_id company_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE office_member DROP FOREIGN KEY FK_9D5522FBFFA0C224');
        $this->addSql('DROP TABLE office');
        $this->addSql('DROP TABLE office_member');
        $this->addSql('ALTER TABLE company CHANGE nb_employees nb_employees INT DEFAULT NULL');
        $this->addSql('ALTER TABLE member CHANGE company_id company_id INT DEFAULT NULL');
    }
}
