<?php


namespace App\Controller;


use App\Entity\Office;
use App\Form\Office\CreateOfficeType;
use App\Repository\OfficeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OfficeController extends AbstractController
{

    /**
     * @var OfficeRepository
     */
    private $or;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * OfficeController constructor.
     * @param OfficeRepository $or
     * @param EntityManagerInterface $em
     */
    public function __construct(OfficeRepository $or, EntityManagerInterface $em)
    {
        $this->or = $or;
        $this->em = $em;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route(path="/offices", name="office_read")
     */
    public function read()
    {
//        $offices = $or->findAllWithAll()
        $offices = $this->or->findAllCustom();

         return $this->render('office/read.html.twig', [
             'offices' => $offices
         ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Route(path="/offices/create", name="office_create")
     */
    public function create(Request $request)
    {
        $office = new Office();

        $form = $this->createForm(CreateOfficeType::class, $office);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($office);
            $this->em->flush();

            $this->addFlash('success', 'Nouveau local OK');
            return $this->redirectToRoute('office_read');
        }

        return $this->render('office/create.html.twig', [
            'form' => $form->createView(), 'office' => $office
        ]);
    }
}