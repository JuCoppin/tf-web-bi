<?php

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    private $Active;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     */
    private $NbEmployees;

    /**
     * @ORM\OneToMany(targetEntity=Member::class, mappedBy="Company")
     */
    private $members;

    /**
     * @ORM\OneToMany(targetEntity=Office::class, mappedBy="Company")
     */
    private $offices;

    public function __toString()
    {
        return $this->getName() . ' - ' . $this->getNbEmployees();
    }

    public function getAsLabel()
    {
        return $this->NbEmployees;
    }

    public function __construct()
    {
        $this->members = new ArrayCollection();
        $this->offices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->Active;
    }

    public function setActive(bool $Active): self
    {
        $this->Active = $Active;

        return $this;
    }

    public function getNbEmployees(): ?int
    {
        return $this->NbEmployees;
    }

    public function setNbEmployees(?int $NbEmployees): self
    {
        $this->NbEmployees = $NbEmployees;

        return $this;
    }

    /**
     * @return Collection|Member[]
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    public function addMember(Member $member): self
    {
        if (!$this->members->contains($member)) {
            $this->members[] = $member;
            $member->setCompany($this);
        }

        return $this;
    }

    public function removeMember(Member $member): self
    {
        if ($this->members->contains($member)) {
            $this->members->removeElement($member);
            // set the owning side to null (unless already changed)
            if ($member->getCompany() === $this) {
                $member->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Office[]
     */
    public function getOffices(): Collection
    {
        return $this->offices;
    }

    public function addOffice(Office $office): self
    {
        if (!$this->offices->contains($office)) {
            $this->offices[] = $office;
            $office->setCompany($this);
        }

        return $this;
    }

    public function removeOffice(Office $office): self
    {
        if ($this->offices->contains($office)) {
            $this->offices->removeElement($office);
            // set the owning side to null (unless already changed)
            if ($office->getCompany() === $this) {
                $office->setCompany(null);
            }
        }

        return $this;
    }
}
