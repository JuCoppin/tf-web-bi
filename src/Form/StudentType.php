<?php

namespace App\Form;

use App\Entity\Student;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class StudentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Student $data */
        $data = $builder->getData();
        dump($data);
        $isCreate = $data->getId() === null;

        if ($isCreate) {
            $builder->add('username', TextType::class, [
                'label' => 'Nom utilisateur',
                'required' => true,
                'constraints' => [
                    new NotBlank()
                ]
            ]);
        } else {
            $builder->add('username', TextType::class, [
                'label' => 'Nom utilisateur',
                'disabled' => true,
            ]);
        }

        $builder->add('email', RepeatedType::class, [
            'type' => EmailType::class,
            'first_options' => [
                'label' => 'Email'
            ],
            'second_options' => [
                'label' => 'Confirmer email'
            ],
            'label' => 'Email',
            'required' => true,
            'constraints' => [
                new NotBlank(), new Email()
            ]
        ]);

        $builder->add('birthdate', DateType::class, array(
            'label' => 'Date de naissance',
//            'widget' => 'single_text',
//            'format' => 'dd-MM-yyyy',
            'required' => true,
            'constraints' => [
                new NotBlank(),
//                new Date()
            ]
        ));

        $builder->add('roles', ChoiceType::class, [
            'label' => 'Roles',
            'required' => false,
            'choices' => [
                'ROLE_ADMIN' => 'ROLE_ADMIN',
                'ROLE_CLIENT' => 'ROLE_CLIENT'
            ],
            'multiple' => true,
            'expanded' => true
        ]);

        if ($isCreate) {
            $builder->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => [
                    'label' => 'Mdp',
                    'attr' => ['pattern' => '.{8}'],
                ],
                'second_options' => [
                    'label' => 'MDP 2',
                    'attr' => ['pattern' => '.{8}'],
                ],
                'required' => true,
                'constraints' => [
                    new NotBlank(), new Regex(['pattern' => '/^.{8}$/'])
                ]
            ]);
        } else {
            $builder->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => [
                    'label' => 'Mdp',
                ],
                'second_options' => [
                    'label' => 'MDP 2',
                ],
                'required' => false,
                'constraints' => []
            ]);
        }

        $builder->add('save', SubmitType::class, array(
            'label' => 'Sauver',
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Student::class,
        ]);
    }
}
