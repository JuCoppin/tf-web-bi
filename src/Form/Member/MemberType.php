<?php

namespace App\Form\Member;

use App\Entity\Company;
use App\Entity\Member;
use App\Entity\Office;
use App\Repository\CompanyRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class MemberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('lastName', TextType::class, [
            'constraints' => [
                new NotBlank(), new Length(['min' => 2])
            ]
        ]);
        $builder->add('firstName', TextType::class, [
            'constraints' => [
                new NotBlank(), new Length(['min' => 2])
            ]
        ]);
        /*
         * Choice label si non fourni => __toString()
         * Si fourni => prop, getProp(), isProp(), __get(prop)
         */
        $builder->add('company', EntityType::class, [
            'class' => Company::class,
//            'choice_label' => 'name',
//            'choice_label' => 'getAsLabel',
            'choice_label' => function (Company $company) {
//                return '--- ' . $company->getName() . ' ---';
                return '--- ' . $company->__toString() . ' ---';
            },
//            'multiple' => true,
//            'expanded' => true,
            'attr' => [
                'class' => 'maClasse'
            ],
            'query_builder' => function (CompanyRepository $cr) {
                // La méthode utilisée DOIT vous renvoyer un QueryBuilder
                // La méthode utilisée par un query_builder DOIT se terminer par Query
                return $cr->findActiveCompaniesQuery();
            }
        ]);

        $builder->add('offices', EntityType::class, [
            'class' => Office::class,
            'label' => 'Locaux',
            'choice_label' => function(Office $office) {
                return $office->getName() . ' - ' . $office->getSize();
            },
            'multiple' => true,
            'expanded' => true,
            'by_reference' => false
        ]);

        $builder->add('number', TextType::class, [
            'attr' => ['placeholder' => '0000.00aB', 'pattern' => '\d{4}\.\d{2}[a-zA-Z]{2}'],
            'label' => 'Numéro EMPLOYE',
            'required' => true,
            'constraints' => [
                new NotBlank(), new Regex(['pattern' => '/^\d{4}\.\d{2}[a-zA-Z]{2}$/', 'message' => 'LUL'])
            ],
            'help' => '0000.00aB'
        ]);

        $builder->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Member::class,
        ]);
    }
}
