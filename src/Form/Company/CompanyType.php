<?php


namespace App\Form\Company;


use App\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class CompanyType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('name', TextType::class, [
            'constraints' => [
                new NotBlank(), new Length(['min' => 2])
            ],
            'required' => true
        ]);
        $builder->add('nbEmployees', IntegerType::class, [
            'constraints' => [
                new NotBlank(), new GreaterThanOrEqual(['value' => 0])
            ]
        ]);
        $builder->add('active', CheckboxType::class, [
            'required' => false,
            'constraints' => []
        ]);

        $builder->add('EnormeSUBMITBUTTON', SubmitType::class, [
            'label' => 'Licorne',
            'attr' => ['class' => 'button btn btn-michel']
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefault("data_class", Company::class);
        $resolver->setDefault('attr', ['novalidate' => true, 'class' => 'super-form']);
    }
}