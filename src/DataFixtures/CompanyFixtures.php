<?php

namespace App\DataFixtures;

use App\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CompanyFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < random_int(1, 1000); $i++) {
            $company = new Company();
            $company->setName($faker->company);
            $company->setNbEmployees($faker->numberBetween(1, 50));
            $company->setActive($faker->boolean(42));

            $manager->persist($company);
        }

        $manager->flush();
    }
}
